import cmd
import locale
import os
import pprint
import shlex
import errno
import datetime
import time
import logging
import argparse

from dropbox import client, rest, session
from dropbox.rest import ErrorResponse
from dropbox.rest import RESTSocketError

from config import Config

ACCESS_TYPE = 'dropbox'  # should be 'dropbox' or 'app_folder' as configured for your app
TOKEN_STORAGE_FILE = 'token_store.cfg'

quiet = False

class BackupUtils():
  """a tool collection to do a recursive download of the whole dropbox for backup usage""" 

  def __init__(self, source_path, destination_path, skip_existing, delete_local):
    self.source_path = source_path
    self.destination_path = destination_path
    self.skip_existing = skip_existing
    self.delete_local = delete_local

    if os.path.isfile(TOKEN_STORAGE_FILE):
      with open(TOKEN_STORAGE_FILE, 'r') as f:
        token_store = Config(f)
        info('[token store file read]')
    else:
      token_store = Config()

    if 'appkey' not in token_store or 'appsecret' not in token_store:
      print 'You need to set your APP_KEY and APP_SECRET!\nYou can find these at http://www.dropbox.com/developers/apps'
      print 'APPKEY:'
      token_store.appkey = raw_input()
      print 'APPSECRET:'
      token_store.appsecret = raw_input()

    self.sess = session.DropboxSession(token_store.appkey, token_store.appsecret, ACCESS_TYPE)
    info('[session created]')
    try:
      self.sess.set_token(token_store.token_key, token_store.token_secret)
      info('[loaded access token]')
    except:
      request_token = self.sess.obtain_request_token()
      url = self.sess.build_authorize_url(request_token)
      print "url:", url
      print "Please authorize in the browser. After you're done, press enter."
      raw_input()

      self.sess.obtain_access_token(request_token)
      info('[loaded access token]')
      token_store.token_key = self.sess.token.key
      token_store.token_secret = self.sess.token.secret
      with open(TOKEN_STORAGE_FILE, 'w+') as f:
        token_store.save(f)
    self.api_client = client.DropboxClient(self.sess)

  def ensure_dir_exists(self, path_to_directory):
    os_path_to_directory = os.path.join(self.destination_path, path_to_directory)
    if not os.path.isdir(os_path_to_directory):
      info("Create %s" % os_path_to_directory)
      os.makedirs(os_path_to_directory)

  def api_get_file(self, full_path_to_file):
    return self.api_client.get_file(full_path_to_file)

  def download_file(self, path_to_file, bytes_online):
    os_file_path = os.path.expanduser(os.path.join(self.destination_path, path_to_file))
    if self.skip_existing and os.path.isfile(os_file_path) and os.path.getsize(os_file_path) == bytes_online:
      info('Skipping %s' % path_to_file)
      return
  
    info('Downloading %s' % path_to_file)
    (os_dir_path, tail) = os.path.split(path_to_file)
    self.ensure_dir_exists(os_dir_path)
    with open(os_file_path, "wb") as file_handle:
      full_path_to_file = os.path.join(self.source_path, path_to_file)
      f = self.api_call_with_attempts(lambda x: self.api_get_file(x), full_path_to_file)
      file_handle.write(f.read())

  def api_metadata(self, os_path_to_directory):
    resp = self.api_client.metadata(os_path_to_directory)
    if 'contents' in resp:
      return resp
    else:
      raise ValueError

  def download_directory(self, path_to_directory):
    full_path_to_directory = os.path.join(self.source_path, path_to_directory)
    directory_content = []

    resp = self.api_call_with_attempts(lambda x: self.api_metadata(x), full_path_to_directory)
    for file_object in resp['contents']:
      filename = os.path.basename(file_object['path'])
      directory_content.append(filename)
      complete_file_path = os.path.join(path_to_directory, filename)
      if file_object['is_dir']:
        self.ensure_dir_exists(complete_file_path)
        self.download_directory(complete_file_path)
      else:
        self.download_file(complete_file_path, file_object['bytes'])
    if self.delete_local:
      os_path_to_directory = os.path.join(self.destination_path, path_to_directory)
      for item in os.listdir(os_path_to_directory):
        if not item in directory_content:
          os_file_path = os.path.join(os_path_to_directory, item).encode('utf-8')
          info("Delete %s" % os_file_path)
          if os.path.isfile(os_file_path):
            os.remove(os_file_path)
          else:
            os.rmdir(os_file_path)

  def api_call_with_attempts(self, call, param):
    # try to download 5 times to handle http 5xx errors from dropbox
    for attempts in range(5):
      try:
        return call(param)
      except (rest.ErrorResponse, rest.RESTSocketError, ValueError) as error:
        error('An error occured while listing a directory. Will try again in some seconds.')
        debug('%s' % str(error))
        time.sleep(attempts*10+5)
    return False

def main():
  now = datetime.datetime.now()
  date_string = now.strftime('%Y-%m-%d_%H-%M')
  parser = argparse.ArgumentParser(
    description='Backup a dropbox profile',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )
  parser.add_argument('--path-dropbox', 
    default='', 
    help = "source path to backup from"
  )
  parser.add_argument('--path-local',
    default=os.path.join('dropbox_backup_' + date_string, ''), 
    help = "destination path to backup to"
  )
  parser.add_argument('--skip-existing',
    dest='skip_existing', 
    action='store_true', 
    default=False, 
    help = "skip existing files"
  )
  parser.add_argument('--delete-local-files', 
    dest='delete_local', 
    action='store_true', 
    default=False, 
    help = "delete local files, which are not online"
  )
  parser.add_argument('--log-file-path', 
    default='', 
    help = "where to save log files"
  )
  parser.add_argument('--quiet',
    dest='quiet',
    action='store_true',
    default=False,
    help = "disable info output"
  )
  args = parser.parse_args()

  global quiet
  quiet = args.quiet

  logging.basicConfig(
    filename=args.log_file_path + 'dropbox_backup_' + date_string + '.log',
    level=logging.DEBUG
  )  

  backup_client = BackupUtils(args.path_dropbox, args.path_local, args.skip_existing, args.delete_local)
  backup_client.download_directory('')

def info(string):
  if not quiet:
    print string
  logging.debug(string)

def debug(string):
  logging.debug(string)

def error(string):
  print string
  logging.error(string)


if __name__ == '__main__':
    main()
