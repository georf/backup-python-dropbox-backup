#Python Dropbox Backup

## Why use this script?
This is a python2 script to backup your whole dropbox via the Dropbox REST-API. This is especially useful for using Dropbox on the Raspberry Pi or any other ARM based system as Dropbox does not provide an ARM version of their official client. You can also use this script as a simple alternative to the complicated Dropbox setup on a server without x-server installed (on X86 based systems).

## What are the requirements?

Note: This is a Python2 (Python2.7) script. It will not run with an up to date Python3 installation.

This script requires the Dropbox Python SDK. It can be downloaded here:
	
https://www.dropbox.com/developers/reference/sdk

You can also use the python package installer:

`pip install dropbox`

Also the config package from python package index is required. Install via:

`pip install config`

## How to use?

`python2.7 backup_dropbox.py`

When launched for the first time it will ask you to enter an APPKEY and an APPSECRET. You'll need to register with Dropbox to get an API key:

https://www.dropbox.com/developers/apps

If you have done that you will be asked to open an URL in your browser to grant the backup script access to your dropbox.

You only have to do all this for the first time. All keys will be stored in a keyfile. If you need to do changes to this data later either edit the files manually or remove the file and run through the setup process of the script again.

If you experience problems with your old keystorage files, please delete it and run again through the setup process.

You're done! The script will now create a new folder with the current date and download the whole dropbox into that folder. Time to get a coffee ;)

## What will be done in the future?

* easy setup with setuptools
* use dropbox delta command to be able to only download changed files

## Further feature requests?

Please leave me a message!
